import os

LOOP_EVERY_S = 60 * 5 # 5 minutes

GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN")

JOB_STATUS_ALL = ['created', 'pending', 'running', 'failed', 'success', 'canceled', 'skipped', 'manual']
JOB_STATUS_FINISHED = ['failed', 'success', 'canceled', 'skipped', 'manual'] # Manual is paused until run
JOB_STATUS_INT = {
    'created':  0,
    'pending':  1,
    'running':  2,
    'failed':   3,
    'success':  4,
    'canceled': 5,
    'skipped':  6,
    'manual':   7
}

SQLITE_DB_PATH = 'database.db'
SQLITE_DB_ISOLATION = None

SQL_CREATE_TABLE = "CREATE TABLE if not exists pipelines(   \
    project_id      INTEGER,                                \
    pipeline_id     INTEGER,                                \
    pending         BOOLEAN DEFAULT 1,                      \
    UNIQUE(project_id, pipeline_id)                         \
    )"
SQL_INSERT_PIPELINE = "INSERT INTO pipelines (project_id, pipeline_id) VALUES (?,?)"
SQL_SELECT_PIPELINES_PENDING = "SELECT project_id, pipeline_id FROM pipelines WHERE pending = 1"
SQL_UPDATE_PIPELINE_MARK_NOT_PENDING = "UPDATE pipelines SET pending = 0 WHERE project_id = ? AND pipeline_id = ?"

INFLUX_HOST = '127.0.0.1'
INFLUX_PORT = 8086
INFLUX_USER = 'root'
INFLUX_PW = 'root'
INFLUX_DB = 'salsa_metrics'
INFLUX_MEASUREMENT_JOBS = 'jobs'
INFLUX_MEASUREMENT_PIPELINES = 'pipelines'
